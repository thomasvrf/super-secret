import {
  getDaysInWeek,
  getEndOfDay,
  parseDateToIRailsFormat,
  secondsToHumanReadable,
} from '@/utils/date';

/*
 * getDaysInWeek
 */

test('getDaysInWeek: stays in month: from sunday (0)', () => {
  // 10/12/2017 (sunday)
  const now = new Date(2017, 11, 10);
  const days = getDaysInWeek(now);

  days.forEach((day, i) => {
    expect(days[i].getDate()).toBe((4 + i));
  });
});

test('getDaysInWeek: stays in month: from weekday (n)', () => {
  // 10/12/2017 (friday)
  const now = new Date(Date.UTC(2017, 11, 8));
  const days = getDaysInWeek(now);

  days.forEach((day, i) => {
    expect(days[i].getDate()).toBe((4 + i));
  });
});

test('getDaysInWeek: adjust month -1', () => {
  // 03/12/2017 (rollback to 11)
  const now = new Date(2017, 11, 3);
  const days = getDaysInWeek(now);

  expect(days[3].getMonth()).toBe(10);
  expect(days[4].getMonth()).toBe(11);
});

test('getDaysInWeek: adjust month +1', () => {
  // 28/11/2017 (goto 12)
  const now = new Date(2017, 10, 28);
  const days = getDaysInWeek(now);

  expect(days[3].getMonth()).toBe(10);
  expect(days[4].getMonth()).toBe(11);
});

/*
 * getEndOfDay
 */

test('getEndOfDay: get end of day in i-rails format 1', () => {
  // start at 9, work 420 minutes
  const eod = getEndOfDay((9 * 60), 420);
  expect(eod).toBe('1600');
});

test('getEndOfDay: get end of day in i-rails format 2', () => {
  // start at 9, work 420 minutes, with 60 minute lunch time
  const eod = getEndOfDay((9 * 60), 420, 60);
  expect(eod).toBe('1700');
});

/*
 * parseDateToIRailsFormat
 */

test('parseDateToIRailsFormat: parse date to i-rails format', () => {
  // start at 9, work 420 minutes, with 60 minute lunch time
  const now = new Date(2017, 11, 10);
  const formatted = parseDateToIRailsFormat(now);

  expect(formatted).toBe('101217');
});

/*
 * secondsToHumanReadable
 */

test('secondsToHumanReadable: prepend 0 to minutes, add h:m indicators', () => {
  const time = secondsToHumanReadable(420);

  expect(time).toBe('00h:07m');
});

test('secondsToHumanReadable: prepend 0 to hours, add h:m indicators', () => {
  const time = secondsToHumanReadable(5580);

  expect(time).toBe('01h:33m');
});
