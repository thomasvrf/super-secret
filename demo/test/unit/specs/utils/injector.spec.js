import {
  setInjectable,
  getInjectable,
} from '@/utils/injector';

const injectable = 'INJECTABLE';

test('can set/get injectable', () => {
  setInjectable(injectable, injectable);
  expect(getInjectable(injectable)).toBe(injectable);
});
