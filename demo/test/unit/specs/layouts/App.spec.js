import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import AppLayout from '@/layouts/App';

test('should have data properties defined', () => {
  const component = shallow(AppLayout);

  expect(component.vm.menu).toEqual({
    overview: 'overview',
    logout: 'sign out',
  });
});

test('should fill slot', () => {
  const component = shallow(AppLayout, {
    slots: {
      default: '<div id="mock-default-slot"></div>',
    },
  });

  expect(component.findAll('#mock-default-slot').length).toBe(1);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(AppLayout, {
    slots: {
      default: '<div id="mock-default-slot"></div>',
    },
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
