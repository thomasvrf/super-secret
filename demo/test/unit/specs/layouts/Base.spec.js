import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import BaseLayout from '@/layouts/Base';

test('should render slots', () => {
  const component = shallow(BaseLayout, {
    slots: {
      default: '<div id="mock-default-slot"></div>',
    },
  });

  expect(component.findAll('#mock-default-slot').length).toBe(1);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(BaseLayout, {
    slots: {
      default: '<div id="mock-default-slot"></div>',
    },
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
