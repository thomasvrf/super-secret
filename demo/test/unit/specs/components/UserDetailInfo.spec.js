import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import UserDetailInfo from '@/components/UserDetailInfo';

const propsMock = {
  status: 'IDLE',
  user: {
    name: 'name',
    address: {
      line_1: 'line_1',
      zipcode: 'zipcode',
      city: 'city',
    },
  },
};

test('should have data properties defined', () => {
  const component = shallow(UserDetailInfo, {
    propsData: propsMock,
  });

  expect(component.vm.title).toBe('Address');
});

test('should have all properties defined', () => {
  const component = shallow(UserDetailInfo, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['status', 'user']);
});

test('should set all properties', () => {
  const component = shallow(UserDetailInfo, {
    propsData: propsMock,
  });

  expect(component.props().user).toEqual(propsMock.user);
  expect(component.props().status).toBe(propsMock.status);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(UserDetailInfo, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
