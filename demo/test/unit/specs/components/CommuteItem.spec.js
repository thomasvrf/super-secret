import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import CommuteItem from '@/components/CommuteItem';

const propsMock = {
  connection: {
    day: 'day',
    arrival: {}, // departure.station && duration && arrival.station
    departure: {}, // departure.station && duration
  },
};

test('should have all properties defined', () => {
  const component = shallow(CommuteItem, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['connection']);
});

test('should set all properties', () => {
  const component = shallow(CommuteItem, {
    propsData: propsMock,
  });

  expect(component.props().connection).toEqual(propsMock.connection);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(CommuteItem, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
