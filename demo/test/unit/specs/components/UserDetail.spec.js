import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import UserDetail from '@/components/UserDetail';

test('should render slots', () => {
  const component = shallow(UserDetail, {
    slots: {
      info: '<div id="mock-info-slot"></div>',
      commute: '<div id="mock-commute-slot"></div>',
    },
  });

  expect(component.findAll('#mock-info-slot').length).toBe(1);
  expect(component.findAll('#mock-commute-slot').length).toBe(1);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(UserDetail, {
    slots: {
      info: '<div id="mock-info-slot"></div>',
      commute: '<div id="mock-commute-slot"></div>',
    },
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
