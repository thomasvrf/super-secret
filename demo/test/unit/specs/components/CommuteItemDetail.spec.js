import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import CommuteItemDetail from '@/components/CommuteItemDetail';
import * as dateUtils from '@/utils/date';

const propsMock = {
  details: {
    departure: { station: 'station' },
    arrival: { station: 'station' },
    duration: 3600,
  },
};

test('should have all properties defined', () => {
  const component = shallow(CommuteItemDetail, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['details']);
});

test('should have all computed props defined', () => {
  const component = shallow(CommuteItemDetail, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.computed))
    .toEqual(['duration']);
});

test('should set all properties', () => {
  const component = shallow(CommuteItemDetail, {
    propsData: propsMock,
  });

  expect(component.props().details).toEqual(propsMock.details);
});

test('should compute properties', () => {
  const spy = jest.spyOn(dateUtils, 'secondsToHumanReadable');

  const component = shallow(CommuteItemDetail, {
    propsData: propsMock,
  });

  expect(spy).toHaveBeenCalled();
  expect(component.vm.duration).toBe('01h:00m');
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(CommuteItemDetail, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
