import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import UserList from '@/components/UserList';

const propsMock = {
  count: 1,
  loadmore: false,
  status: 'IDLE',
  users: [],
};

test('should have all properties defined', () => {
  const component = shallow(UserList, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['count', 'loadmore', 'status', 'users']);
});

test('should have all methods defined', () => {
  const component = shallow(UserList, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.methods))
    .toEqual(['loadMoreItems']);
});

test('should set all properties', () => {
  const component = shallow(UserList, {
    propsData: propsMock,
  });

  expect(component.props().count).toBe(1);
  expect(component.props().loadmore).toBe(false);
  expect(component.props().status).toBe('IDLE');
  expect(component.props().users).toEqual([]);
});

test('should emit userlist:loadmore on loadMoreItems', () => {
  const component = shallow(UserList, {
    propsData: { ...propsMock, loadmore: true },
  });

  const btnLoadMore = component.find('#btn-load-more');
  btnLoadMore.trigger('click');
  expect(component.emitted('userlist:loadmore')).toBeTruthy();
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(UserList, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
