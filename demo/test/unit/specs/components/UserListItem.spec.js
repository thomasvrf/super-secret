import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import UserListItem from '@/components/UserListItem';

const propsMock = {
  user: {
    id: 1,
    name: 'name',
    email: 'email',
    role_name: 'role_name',
  },
};

test('should have all properties defined', () => {
  const component = shallow(UserListItem, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['user']);
});

test('should set all properties', () => {
  const component = shallow(UserListItem, {
    propsData: propsMock,
  });

  expect(component.props().user).toEqual(propsMock.user);
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(UserListItem, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
