import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import UserDetailCommute from '@/components/UserDetailCommute';
import * as dateUtils from '@/utils/date';

const connection = { departure: { duration: 3600 }, arrival: { duration: 3600 } };

const propsMock = {
  status: 'SUCCESS',
  connections: [connection, connection],
};

test('should have data properties defined', () => {
  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  expect(component.vm.title).toBe('Commute');
});

test('should have all properties defined', () => {
  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.props))
    .toEqual(['status', 'connections']);
});

test('should have all computed props defined', () => {
  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  expect(Object.keys(component.vm.$options.computed))
    .toEqual(['total']);
});

test('should set all properties', () => {
  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  expect(component.props().connections).toEqual(propsMock.connections);
  expect(component.props().status).toBe(propsMock.status);
});

test('should compute properties', () => {
  const spy = jest.spyOn(dateUtils, 'secondsToHumanReadable');

  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  expect(spy).toHaveBeenCalled();
  expect(component.vm.total).toBe('04h:00m');
});

test('should compute properties even duration missing', () => {
  const spy = jest.spyOn(dateUtils, 'secondsToHumanReadable');

  const dirtyConnection = { departure: {}, arrival: {} };

  const component = shallow(UserDetailCommute, {
    propsData: { ...propsMock, connections: [dirtyConnection, dirtyConnection] },
  });

  expect(spy).toHaveBeenCalled();
  expect(component.vm.total).toBe('00h:00m');
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(UserDetailCommute, {
    propsData: propsMock,
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
