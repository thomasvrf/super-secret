import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import AuthenticatePage from '@/pages/Authenticate/Index';
import * as authConnectorMock from '@/connectors/auth';

authConnectorMock.auth = jest.fn(() => Promise.resolve());

test('has a created lifecycle hook', () => {
  const component = shallow(AuthenticatePage, {
    mocks: {
      $route: { query: { code: 'routeQueryCode' } },
    },
  });
  expect(typeof component.vm.$options.created[0]).toBe('function');
});

test('should call auth with queryCode', () => {
  shallow(AuthenticatePage, {
    mocks: {
      $route: { query: { code: 'routeQueryCode' } },
    },
  });

  expect(authConnectorMock.auth).toBeCalledWith('routeQueryCode');
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(AuthenticatePage, {
    mocks: {
      $route: { query: { code: 'routeQueryCode' } },
    },
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
