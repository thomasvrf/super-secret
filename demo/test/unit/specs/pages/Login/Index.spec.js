import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import LoginPage from '@/pages/Login/Index';

test('should have data properties defined', () => {
  const component = shallow(LoginPage);
  expect(component.vm.title).toEqual('Commute sheet');
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(LoginPage);

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
