import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import IndexPage from '@/pages/Index/Index';
import * as userConnectorMock from '@/connectors/users';
import * as injectorMock from '@/utils/injector';

const usersState = {
  status: 'IDLE',
  pagesLoaded: 0,
  limit: 30,
  totalRows: 0,
  users: [],
  query: '',
};

userConnectorMock.fetchInitialUsers = jest.fn();
injectorMock.getInjectable = () => ({
  dispatch: jest.fn(),
  state: {
    users: usersState,
  },
});

test('should have data properties defined', () => {
  const component = shallow(IndexPage);
  expect(component.vm.title).toBe('Users');
});

test('should have all computed props defined', () => {
  const component = shallow(IndexPage);

  expect(Object.keys(component.vm.$options.computed))
    .toEqual(['users', 'pages', 'status', 'loadmore', 'query']);
});

test('should have a created lifecycle hook', () => {
  const component = shallow(IndexPage);
  expect(typeof component.vm.$options.created[0]).toBe('function');
});

test('should call fetchInitialUsers when created', () => {
  shallow(IndexPage);
  expect(userConnectorMock.fetchInitialUsers).toBeCalled();
});

test('should not call fetchInitialUsers when already fetched', () => {
  usersState.status = 'SUCCESS';
  shallow(IndexPage);
  expect(userConnectorMock.fetchInitialUsers).not.toBeCalled();
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(IndexPage);

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
