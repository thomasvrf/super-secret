import { shallow } from 'vue-test-utils';
import { createRenderer } from 'vue-server-renderer';
import DetailPage from '@/pages/Detail/Index';
import * as injectorMock from '@/utils/injector';
import * as userConnectorMock from '@/connectors/user';

const userState = {
  status: 'IDLE',
  user: {},
};

const commuteState = {
  status: 'IDLE',
  commutes: [],
};

userConnectorMock.fetchUser = jest.fn();
injectorMock.getInjectable = () => ({
  dispatch: jest.fn(),
  state: {
    user: userState,
    commute: commuteState,
  },
});

test('should have data properties defined', () => {
  const component = shallow(DetailPage, {
    mocks: {
      $route: { params: { id: 'routeParamId' } },
    },
  });
  expect(component.vm.title).toBe('Commute sheet');
});

test('should have all computed props defined', () => {
  const component = shallow(DetailPage, {
    mocks: {
      $route: { params: { id: 'routeParamId' } },
    },
  });

  expect(Object.keys(component.vm.$options.computed))
    .toEqual(['user', 'connections', 'userInfoStatus', 'userCommuteStatus']);
});

test('should have a created lifecycle hook', () => {
  const component = shallow(DetailPage, {
    mocks: {
      $route: { params: { id: 'routeParamId' } },
    },
  });
  expect(typeof component.vm.$options.created[0]).toBe('function');
});

test('should call fetchUser on created', () => {
  shallow(DetailPage, {
    mocks: {
      $route: { params: { id: 'routeParamId' } },
    },
  });
  expect(userConnectorMock.fetchUser).toBeCalledWith('routeParamId');
});

/*
 * Snapshots
 */

test('matches snapshot', () => {
  const renderer = createRenderer();
  const component = shallow(DetailPage, {
    mocks: {
      $route: { params: { id: 'routeParamId' } },
    },
  });

  renderer.renderToString(component.vm, (err, str) => {
    if (err) throw new Error(err);
    expect(str).toMatchSnapshot();
  });
});
