import {
  setTokens,
  setAccessToken,
  removeTokens,
  getAccessToken,
  getRefreshToken,
  hasAccessToken,
  hasRefreshToken,
} from '@/services/local-storage/tokens';
import localStorageMock from '../../../mocks/localStorage';

global.localStorage = localStorageMock;

const accessToken = 'accessToken';
const refreshToken = 'refreshToken';

test('can set/get tokens', () => {
  setTokens(accessToken, refreshToken);
  expect(getAccessToken()).toEqual(accessToken);
  expect(getRefreshToken()).toEqual(refreshToken);
});

test('can set/get access token individually', () => {
  setAccessToken('newAccessToken');
  expect(getAccessToken()).toEqual('newAccessToken');
});

test('can remove tokens', () => {
  removeTokens();
  // getting removed tokens returns empty string
  expect(getAccessToken()).toEqual('');
  expect(getRefreshToken()).toEqual('');
});

test('can check if tokens are set: false', () => {
  expect(hasAccessToken()).toEqual(false);
  expect(hasRefreshToken()).toEqual(false);
});

test('can check if tokens are set: true', () => {
  setTokens(accessToken, refreshToken);
  expect(hasAccessToken()).toEqual(true);
  expect(hasRefreshToken()).toEqual(true);
});
