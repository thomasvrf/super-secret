import {
    get,
    set,
    remove,
} from '@/services/local-storage/index';
import localStorageMock from '../../../mocks/localStorage';

global.localStorage = localStorageMock;

const storage = 'storage';

test('can set in localstorage', () => {
  set(storage, storage);
  expect(get(storage)).toEqual(storage);
});

test('can get from localstorage', () => {
  expect(get(storage)).toEqual(storage);
});

test('can remove from localstorage', () => {
  remove(storage);
  expect(get(storage)).toEqual(null);
});
