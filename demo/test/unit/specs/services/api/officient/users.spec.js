import {
  getUsers,
  getUser,
} from '@/services/api/officient/users';
import * as mapMock from '@/mappers/users';
import * as apiMock from '@/services/api/officient/api';

const dataResponse = ['data1', 'data2'];
const mappedResponse = 'mapped';
apiMock.apiRequest = jest.fn(() => ({ data: dataResponse }));
mapMock.mapToUsersData = jest.fn(() => mappedResponse);
mapMock.mapToUserDetail = jest.fn(() => mappedResponse);

test('getUsers: should create correct api call', async () => {
  await getUsers(1);
  const users = '/people/list?page=1';
  expect(apiMock.apiRequest).toBeCalledWith(users, {
    method: 'GET',
  });
});

test('getUsers: should call mapper with data', async () => {
  await getUsers(1);
  expect(mapMock.mapToUsersData).toBeCalledWith({ data: dataResponse });
});

test('getUsers: should return mapped result', async () => {
  const users = await getUsers(1);
  expect(users).toBe(mappedResponse);
});

test('getUser: should create correct api call', async () => {
  await getUser(1);
  const user = '/people/1/detail';
  expect(apiMock.apiRequest).toBeCalledWith(user, {
    method: 'GET',
  });
});

test('getUser: should call mapper with data', async () => {
  await getUser(1);
  expect(mapMock.mapToUserDetail).toBeCalledWith(dataResponse);
});

test('getUser: should return mapped results', async () => {
  const user = await getUser(1);
  expect(user).toBe(mappedResponse);
});
