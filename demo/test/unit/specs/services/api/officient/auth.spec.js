import {
  auth,
  refresh,
} from '@/services/api/officient/auth';
import * as apiMock from '@/services/api/officient/api';
import * as tokenMock from '@/services/local-storage/tokens';

const tokenResponse = { access_token: 'access_token', refresh_token: 'refresh_token' };
const refreshResponse = 'refresh_token';
apiMock.apiRequest = jest.fn(() => tokenResponse);
tokenMock.setAccessToken = jest.fn();
tokenMock.getRefreshToken = jest.fn(() => refreshResponse);

const authCode = 'auth_code';

test('auth: should create correct auth api call', async () => {
  await auth(authCode);
  expect(apiMock.apiRequest).toBeCalledWith('/token', {
    method: 'POST',
    body: JSON.stringify({
      code: `${authCode}`,
      grant_type: 'authorization_code',
      refresh_token: '',
    }),
  });
});

test('refresh: should call getRefreshToken', async () => {
  await refresh();
  expect(tokenMock.getRefreshToken).toBeCalled();
});

test('refresh: should create correct call', async () => {
  await refresh();
  expect(apiMock.apiRequest).toBeCalledWith('/token', {
    method: 'POST',
    body: JSON.stringify({
      code: '',
      grant_type: 'refresh_token',
      refresh_token: `${refreshResponse}`,
    }),
  });
});

test('refresh: should set new token in localstorage', async () => {
  await refresh();
  expect(tokenMock.setAccessToken).toBeCalledWith(`${tokenResponse.access_token}`);
});

test('refresh: should set new token in localstorage even when null', async () => {
  apiMock.apiRequest = jest.fn(() => ({ access_token: null, refresh_token: null }));
  await refresh();
  expect(tokenMock.setAccessToken).toBeCalledWith('');
});
