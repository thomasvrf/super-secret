import {
  getUserWage,
} from '@/services/api/officient/wages';
import * as mapMock from '@/mappers/wages';
import * as apiMock from '@/services/api/officient/api';

const dataResponse = ['data1', 'data2'];
const mapResponse = 'mapped';
apiMock.apiRequest = jest.fn(() => ({ data: dataResponse }));
mapMock.mapWageTimeEngagement = jest.fn(() => mapResponse);

test('should create correct api call', async () => {
  await getUserWage(1);
  const wage = '/wages/1/current';
  expect(apiMock.apiRequest).toBeCalledWith(wage, {
    method: 'GET',
  });
});

test('should call mapper with data', async () => {
  await getUserWage(1);
  expect(mapMock.mapWageTimeEngagement).toBeCalledWith(dataResponse);
});

test('should return mapped result', async () => {
  const wage = await getUserWage(1);
  expect(wage).toBe(mapResponse);
});
