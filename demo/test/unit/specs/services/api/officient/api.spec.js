import {
  apiRequest,
} from '@/services/api/officient/api';
import * as httpMock from '@/services/api/http';
import * as authMock from '@/services/api/officient/auth';
import * as tokenMock from '@/services/local-storage/tokens';

const tokenResponse = 'token_response';
httpMock.fetchRefreshRetry = jest.fn(() => Promise.resolve());
authMock.refresh = jest.fn();
tokenMock.getAccessToken = jest.fn(() => 'token_response');

const requestOptions = {
  method: 'GET',
};

test('should call getAccessToken', async () => {
  await apiRequest('/wages', requestOptions);
  expect(tokenMock.getAccessToken).toBeCalled();
});

test('should create the full path to api and and extended requestOptions', async () => {
  await apiRequest('/wages', requestOptions);
  expect(httpMock.fetchRefreshRetry).toBeCalledWith('http://localhost:3001/api/officient/wages', {
    ...requestOptions,
    headers: {
      Authorization: `Bearer ${tokenResponse}`,
      'Content-Type': 'application/json',
    },
  }, authMock.refresh, true);
});

test('should navigate to window location /logout if error 401', async () => {
  window.location.assign = jest.fn();
  httpMock.fetchRefreshRetry = jest.fn(() => Promise.reject({ code: 401 }));

  try {
    await apiRequest('/wages', requestOptions);
  } catch (e) {
    expect(window.location.assign).toBeCalledWith('/logout');
  }
});

test('should throw error again if not 401', async () => {
  httpMock.fetchRefreshRetry = jest.fn(() => Promise.reject('error'));

  try {
    await apiRequest('/wages', requestOptions);
  } catch (e) {
    expect(e).toBe('error');
  }
});
