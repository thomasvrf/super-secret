import {
  fetchRefreshRetry,
} from '@/services/api/http';

global.fetch = jest.fn(() => Promise.resolve({ status: 200, json: jest.fn() }));

test('should fetch requested resource', async () => {
  await fetchRefreshRetry('/connections', { method: 'GET' });
  expect(global.fetch).toBeCalledWith('/connections', { method: 'GET' });
});

test('should throw error on bad status code', async () => {
  global.fetch = jest.fn(() => Promise.resolve({ status: 500, statusText: 'server error', json: jest.fn() }));

  try {
    await fetchRefreshRetry('/connections', { method: 'GET' });
  } catch (e) {
    expect(e).toEqual(new Error('server error'));
  }
});

test('should return json when resolved', async () => {
  global.fetch = jest.fn(() => Promise.resolve({ status: 200, json: jest.fn(() => 'dataobject') }));
  const result = await fetchRefreshRetry('/connections', { method: 'GET' });
  expect(result).toBe('dataobject');
});
