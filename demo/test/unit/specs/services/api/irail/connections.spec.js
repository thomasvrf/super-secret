import {
  getConnections,
} from '@/services/api/irail/connections';
import * as mapMock from '@/mappers/connections';
import * as apiMock from '@/services/api/irail/api';

const connectionResponse = ['connection1', 'connection2'];
const mappedResponse = 'mapped';
apiMock.apiRequest = jest.fn(() => ({ connection: connectionResponse }));
mapMock.mapConnections = jest.fn(() => mappedResponse);

test('should create correct api call', async () => {
  await getConnections('from', 'to', 'date', 'time', 'timesel');
  const connection = '/connections/?from=from&to=to&date=date&time=time&timesel=timesel&format=json&lang=en&results=1';
  expect(apiMock.apiRequest).toBeCalledWith(connection, {
    method: 'GET',
  });
});

test('should call mapper with connection', async () => {
  await getConnections('from', 'to', 'date', 'time', 'timesel');
  expect(mapMock.mapConnections).toBeCalledWith(connectionResponse);
});

test('should return mapped results', async () => {
  const connections = await getConnections('from', 'to', 'date', 'time', 'timesel');
  expect(connections).toBe(mappedResponse);
});
