import {
  apiRequest,
} from '@/services/api/irail/api';
import * as httpMock from '@/services/api/http';

httpMock.fetchRefreshRetry = jest.fn();

const requestOptions = {
  method: 'GET',
};

test('should create the full path to api and proxy requestoptions', async () => {
  await apiRequest('/connections', requestOptions);
  expect(httpMock.fetchRefreshRetry).toBeCalledWith('https://api.irail.be/connections', requestOptions);
});
