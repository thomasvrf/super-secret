import {
  mapConnections,
} from '@/mappers/connections';

const connection = {
  id: 1,
  duration: 1,
  arrival: {
    station: 'station',
    lat: '3',
    lon: '45',
  },
  departure: {
    station: 'station',
    lat: '3',
    lon: '45',
  },
  version: '1.1',
  success: true,
};

const connections = [connection, connection];

test('should remove redundant information', () => {
  const mapped = mapConnections(connections);

  expect(mapped).toEqual([
    {
      id: 1,
      duration: 1,
      arrival: { station: 'station' },
      departure: { station: 'station' },
    },
    {
      id: 1,
      duration: 1,
      arrival: { station: 'station' },
      departure: { station: 'station' },
    },
  ]);
});
