import {
  mapWageTimeEngagement,
} from '@/mappers/wages';

const wages = {
  weekly_time_engagement_minutes: {
    friday: 420,
    saturday: 0,
    // Sunday missing
    monday: 420,
    tuesday: 420,
    wednesday: 420,
    thursday: 420,
  },
};

test('should convert to array', () => {
  const timeEngagement = mapWageTimeEngagement(wages);
  expect(Array.isArray(timeEngagement)).toBe(true);
});

test('should convert to array with objects in format { day, dayDuration }', () => {
  const timeEngagement = mapWageTimeEngagement(wages);
  expect(timeEngagement[0]).toEqual({
    day: 'monday',
    dayDuration: 420,
  });
});

test('should be in order of the week', () => {
  const timeEngagement = mapWageTimeEngagement(wages);
  expect(timeEngagement[0].day).toBe('monday');
  expect(timeEngagement[3].day).toBe('thursday');
  expect(timeEngagement[6].day).toBe('sunday');
});

test('should fill missing values with 0', () => {
  // sunday is missing
  const timeEngagement = mapWageTimeEngagement(wages);
  expect(timeEngagement[6]).toEqual({
    day: 'sunday',
    dayDuration: 0,
  });
});
