import {
  mapToUsersData,
  mapToUserDetail,
} from '@/mappers/users';

const userDetail = {
  id: 1,
  name: 'name',
  email: 'name@name.com',
  address: {
    city: 'city',
    country_code: 'country_code',
    line_1: 'line_1',
    line_2: 'line_2',
    state: 'state',
    zipcode: 'zipcode',
  },
};

test('mapToUserDetail: should remove redundant information', () => {
  const userDetailCloned = { ...userDetail };
  userDetailCloned.redundant0 = '0';
  userDetailCloned.redundant1 = '1';
  const mapped = mapToUserDetail(userDetailCloned);

  expect(mapped).toEqual(userDetail);
});

test('mapToUserDetail: should fill email missing', () => {
  const userDetailDirty = { ...userDetail, email: null };
  const mapped = mapToUserDetail(userDetailDirty);
  expect(mapped.email).toBe('');
});

test('mapToUserDetail: should fill address even when missing', () => {
  const userDetailDirty = { ...userDetail, address: null };
  const mapped = mapToUserDetail(userDetailDirty);

  expect(mapped.address).toEqual({
    city: '',
    country_code: '',
    line_1: '',
    line_2: '',
    state: '',
    zipcode: '',
  });
});

const usersData = {
  redundant: '0',
  total_record_count: 2,
  data: [
    {
      id: 1,
      name: 'name',
      email: 'email@email.com',
      role_name: 'role_name',
    },
    {
      id: 1,
      name: 'name',
      email: null,
      role_name: null,
    },
  ],
};

test('mapToUsersData: should copy properties and fill missing fields', () => {
  const mapped = mapToUsersData(usersData);
  expect(mapped).toEqual({
    total_record_count: 2,
    data: [
      {
        id: 1,
        name: 'name',
        email: 'email@email.com',
        role_name: 'role_name',
      },
      {
        id: 1,
        name: 'name',
        email: '',
        role_name: '',
      },
    ],
  });
});
