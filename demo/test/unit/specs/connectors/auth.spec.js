import {
  hasAuth,
  auth,
  removeTokens,
} from '@/connectors/auth';
import * as authServiceMock from '@/services/api/officient/auth';
import * as tokenServiceMock from '@/services/local-storage/tokens';

const authResponse = { access_token: 'access_token', refresh_token: 'refresh_token' };
tokenServiceMock.hasAccessToken = jest.fn(() => true);
tokenServiceMock.hasRefreshToken = jest.fn(() => false);
tokenServiceMock.removeTokens = jest.fn();
tokenServiceMock.setTokens = jest.fn();
authServiceMock.auth = jest.fn(() => authResponse);

test('hasAuth calls token localstorage', () => {
  hasAuth();
  expect(tokenServiceMock.hasAccessToken).toBeCalled();
  expect(tokenServiceMock.hasRefreshToken).toBeCalled();
});

test('hasAuth return boolean: 1', () => {
  tokenServiceMock.hasAccessToken = jest.fn(() => true);
  tokenServiceMock.hasRefreshToken = jest.fn(() => false);
  expect(hasAuth()).toBe(false);
});

test('hasAuth return boolean: 2', () => {
  tokenServiceMock.hasAccessToken = jest.fn(() => true);
  tokenServiceMock.hasRefreshToken = jest.fn(() => true);
  expect(hasAuth()).toBe(true);
});

test('removeTokens calls token localstorage to remove tokens', () => {
  removeTokens();
  expect(tokenServiceMock.removeTokens).toBeCalled();
});

test('auth calls service with code', async () => {
  await auth(1978);
  expect(authServiceMock.auth).toBeCalledWith(1978);
});

test('if auth ok, set tokens', async () => {
  await auth(1248);
  expect(tokenServiceMock.setTokens).toBeCalledWith(`${authResponse.access_token}`, `${authResponse.refresh_token}`);
});

test('if auth not ok, throw error', async () => {
  authServiceMock.auth = jest.fn(() => ({ error: 'error', error_description: 'error_description' }));

  try {
    await auth(9837);
  } catch (e) {
    expect(e).toEqual(new Error('error_description'));
  }
});
