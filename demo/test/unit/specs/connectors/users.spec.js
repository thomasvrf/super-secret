import {
  fetchInitialUsers,
  fetchMoreUsers,
  getUsers,
  getPages,
  getStatus,
  canLoadMore,
} from '@/connectors/users';
import actionTypes from '@/store/actions';
import * as userServiceMock from '@/services/api/officient/users';
import * as injectorMock from '@/utils/injector';

/*
 * Mock state
 */

const state = {
  users: {
    status: 'IDLE',
    pagesLoaded: 0,
    limit: 30,
    totalRows: 0,
    users: [],
    query: '',
  },
};

const store = {
  dispatch: jest.fn(() => {}),
  state,
};

const usersResponse = {
  total_record_count: 2,
  data: [{ name: 'firstname' }, { name: 'lastname' }],
};

injectorMock.getInjectable = () => store;
userServiceMock.getUsers = jest.fn(() => usersResponse);

/*
 * Getters
 */

test('can get pagesLoaded from store', () => {
  const page = getPages();
  expect(page).toBe(0);
});

test('can get status from store', () => {
  const status = getStatus();
  expect(status).toBe('IDLE');
});

test('can get loadmore, indicates if there are more pages to load 1', () => {
  const canloadMore = canLoadMore();
  expect(canloadMore).toBe(false);
});

test('can get loadmore, indicates if there are more pages to load 2', () => {
  store.state = { users: { ...state.users, pagesLoaded: 1, totalRows: 30 } };
  const canloadMore = canLoadMore();
  expect(canloadMore).toBe(false);
});

test('can get loadmore, indicates if there are more pages to load 3', () => {
  store.state = { users: { ...state.users, pagesLoaded: 1, totalRows: 31 } };
  const canloadMore = canLoadMore();
  expect(canloadMore).toBe(true);
});

test('can get users 1', () => {
  const users = getUsers();
  expect(users).toEqual([]);
});

test('can get users 2', () => {
  store.state = { users: { ...state.users, users: [{ name: 'firstname' }, { name: 'lastname' }] } };
  const users = getUsers();
  expect(users).toEqual([{ name: 'firstname' }, { name: 'lastname' }]);
});

test('can get users: filter in users with query: 1', () => {
  store.state = { users: { ...state.users, users: [{ name: 'firstname' }, { name: 'lastname' }], query: 'name' } };
  const users = getUsers();
  expect(users).toEqual([{ name: 'firstname' }, { name: 'lastname' }]);
});

test('can get users: filter in users with query: 2', () => {
  store.state = { users: { ...state.users, users: [{ name: 'firstname' }, { name: 'lastname' }], query: 'lastname' } };
  const users = getUsers();
  expect(users).toEqual([{ name: 'lastname' }]);
});

/*
 * Actions
 */

// fetchInitialUsers

test('fetchInitialUsers: will call service with page 0', async () => {
  await fetchInitialUsers();
  expect(userServiceMock.getUsers).toBeCalledWith(0);
});

test('fetchInitialUsers: will dispatch REQUEST_USERS and then REQUEST_USERS_SUCCESS with result', async () => {
  userServiceMock.getUsers = jest.fn(() => usersResponse);
  await fetchInitialUsers();
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USERS, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USERS_SUCCESS, {
    users: usersResponse.data,
    record_count: usersResponse.total_record_count,
  }]);
});

test('fetchInitialUsers: will dispatch REQUEST_USERS and then REQUEST_USERS_FAILED if failed', async () => {
  userServiceMock.getUsers = jest.fn(() => { throw new Error('error'); });
  await fetchInitialUsers();
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USERS, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USERS_FAILED, undefined]);
});

// fetchMoreUsers

test('fetchMoreUsers: will call service with requested page', async () => {
  await fetchMoreUsers(1);
  expect(userServiceMock.getUsers).toBeCalledWith(1);
});

test('fetchMoreUsers: will dispatch REQUEST_USERS and then REQUEST_MORE_USERS_SUCCESS with result', async () => {
  userServiceMock.getUsers = jest.fn(() => usersResponse);
  await fetchMoreUsers(1);
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USERS, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_MORE_USERS_SUCCESS, {
    users: usersResponse.data,
    record_count: usersResponse.total_record_count,
  }]);
});

test('fetchMoreUsers: will dispatch REQUEST_USERS and then REQUEST_USERS_FAILED if failed', async () => {
  userServiceMock.getUsers = jest.fn(() => { throw new Error('error'); });
  await fetchMoreUsers(1);
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USERS, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USERS_FAILED, undefined]);
});
