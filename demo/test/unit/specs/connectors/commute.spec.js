import {
  fetchConnections,
  getCommutes,
  userCommuteStatus,
  reset,
} from '@/connectors/commute';
import actionTypes from '@/store/actions';
import * as wageServiceMock from '@/services/api/officient/wages';
import * as connectionServiceMock from '@/services/api/irail/connections';
import * as injectorMock from '@/utils/injector';
import * as dateUtilsMock from '@/utils/date';

/*
 * Mock state
 */

const state = {
  commute: {
    status: 'IDLE',
    commutes: [],
  },
};

const store = {
  dispatch: jest.fn(() => {}),
  state,
};

const dayObj = { day: 'day', dayDuration: 420 };
const wageResponse = Array(7).fill(dayObj);
const daysInWeekResponse = wageResponse;
const IRailsDateResponse = '101217';
const IRailsTimeResponse = '1700';
const connectionsResponse = ['connection1', 'connection2'];
injectorMock.getInjectable = () => store;
wageServiceMock.getUserWage = jest.fn(() => wageResponse);
dateUtilsMock.getDaysInWeek = jest.fn(() => daysInWeekResponse);
dateUtilsMock.parseDateToIRailsFormat = jest.fn(() => IRailsDateResponse);
dateUtilsMock.getEndOfDay = jest.fn(() => IRailsTimeResponse);
connectionServiceMock.getConnections = jest.fn(() => connectionsResponse);

/*
 * Getters
 */

test('can get status from store', () => {
  const status = userCommuteStatus();
  expect(status).toBe('IDLE');
});

test('can get commutes from store 1', () => {
  const commutes = getCommutes();
  expect(commutes).toEqual([]);
});

test('can get commutes from store 2', () => {
  store.state = { commute: { ...state.commute, commutes: ['one', 'two', 'three'] } };
  const commutes = getCommutes();
  expect(commutes).toEqual(['one', 'two', 'three']);
});

/*
 * Actions
 */

test('reset: reset action should dispatch reset', () => {
  reset();
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.RESET_USER_COMMUTE, undefined]);
});

test('fetchConnections: should get wages', async () => {
  await fetchConnections(1, 'Brugge');
  expect(wageServiceMock.getUserWage).toBeCalledWith(1);
});

test('fetchConnections: should get days in week', async () => {
  await fetchConnections(1, 'Brugge');
  expect(dateUtilsMock.getDaysInWeek).toBeCalled();
});

//

test('fetchConnections: should parse date to IRails format', async () => {
  await fetchConnections(1, 'Brugge');
  expect(dateUtilsMock.parseDateToIRailsFormat).toBeCalled();
});

test('fetchConnections: should parse time to IRails format', async () => {
  await fetchConnections(1, 'Brugge');
  expect(dateUtilsMock.getEndOfDay).toBeCalledWith(540, 420, 60);
});

test('fetchConnections: format steps happen 7 times (days)', async () => {
  await fetchConnections(1, 'Brugge');
  expect(dateUtilsMock.getEndOfDay.mock.calls.length).toBe(7);
  expect(dateUtilsMock.parseDateToIRailsFormat.mock.calls.length).toBe(7);
});

//

test('fetchConnections: should call the irails api to get connections', async () => {
  await fetchConnections(1, 'Brugge');

  expect(connectionServiceMock.getConnections.mock.calls[0])
    .toEqual(['Brugge', 'Gent-Sint-Pieters', '101217', '0900', 'arrival']);

  expect(connectionServiceMock.getConnections.mock.calls[1])
    .toEqual(['Gent-Sint-Pieters', 'Brugge', '101217', '1700', 'departure']);
});

test('fetchConnections: fetch step happens 14 times (days x 2 (arrival + departure))', async () => {
  await fetchConnections(1, 'Brugge');
  expect(connectionServiceMock.getConnections.mock.calls.length).toBe(14);
});

//

const connectionSuccessResponse = {
  date: dayObj,
  address: 'Brugge',
  day: dayObj.day,
  hours: dayObj.dayDuration,
  arrival: connectionsResponse[0],
  departure: connectionsResponse[0],
};

const connectionsSuccessResponse = Array(7).fill(connectionSuccessResponse);

test('will dispatch REQUEST_USER_COMMUTE and then REQUEST_USER_COMMUTE_SUCCESS with result', async () => {
  await fetchConnections(1, 'Brugge');

  expect(injectorMock.getInjectable().dispatch.mock.calls[0])
    .toEqual(['REQUEST_USER_COMMUTE', undefined]);

  expect(injectorMock.getInjectable().dispatch.mock.calls[1])
    .toEqual(['REQUEST_USER_COMMUTE_SUCCESS', { commutes: connectionsSuccessResponse }]);
});

test('will dispatch REQUEST_USER_COMMUTE and then REQUEST_USER_COMMUTE_SUCCESS with result', async () => {
  await fetchConnections(1, 'Brugge');

  expect(injectorMock.getInjectable().dispatch.mock.calls[0])
    .toEqual(['REQUEST_USER_COMMUTE', undefined]);

  expect(injectorMock.getInjectable().dispatch.mock.calls[1])
    .toEqual(['REQUEST_USER_COMMUTE_SUCCESS', { commutes: connectionsSuccessResponse }]);
});

test('will dispatch REQUEST_USER_COMMUTE and then REQUEST_USER_COMMUTE_FAILED if failed', async () => {
  wageServiceMock.getUserWage = jest.fn(() => { throw new Error('error'); });
  await fetchConnections(1, 'Brugge');
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USER_COMMUTE, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USER_COMMUTE_FAILED, undefined]);
});
