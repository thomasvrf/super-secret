import {
  fetchUser,
  getUser,
  getUserInfoStatus,
} from '@/connectors/user';
import actionTypes from '@/store/actions';
import * as injectorMock from '@/utils/injector';
import * as userServiceMock from '@/services/api/officient/users';
import * as commuteServiceMock from '@/connectors/commute';

/*
 * Mock state
 */

const state = {
  user: {
    status: 'IDLE',
    user: {},
  },
};

const store = {
  dispatch: jest.fn(() => {}),
  state,
};

const userResponse = {
  address: {
    city: 'Gent',
  },
};

injectorMock.getInjectable = () => store;
userServiceMock.getUser = jest.fn(() => userResponse);
commuteServiceMock.reset = jest.fn(() => {});
commuteServiceMock.fetchConnections = jest.fn(() => {});

/*
 * Getters
 */

test('can get user from store 1', () => {
  const user = getUser();
  expect(user).toEqual({});
});

test('can get user from store 2', () => {
  // (update state)
  state.user.user = { name: 'name' };
  const user = getUser();
  expect(user).toEqual({ name: 'name' });
});

test('can get status from store', () => {
  const status = getUserInfoStatus();
  expect(status).toBe('IDLE');
});

/*
 * Actions
 */

test('will reset commuteService', async () => {
  await fetchUser(1);
  expect(commuteServiceMock.reset).toBeCalled();
});

test('will call user service with id', () => {
  fetchUser(1942);
  expect(userServiceMock.getUser).toBeCalledWith(1942);
});

test('will call fetchConnections with id and responsed address', async () => {
  await fetchUser(300);
  expect(commuteServiceMock.fetchConnections).toBeCalledWith(300, userResponse.address.city);
});

test('will dispatch REQUEST_USER and then REQUEST_USER_SUCCESS with result', async () => {
  await fetchUser(1);
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USER, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USER_SUCCESS, {
    user: userResponse,
  }]);
});

test('will dispatch REQUEST_USER and then REQUEST_USER_FAILED if failed', async () => {
  userServiceMock.getUser = jest.fn(() => { throw new Error('error'); });
  await fetchUser(1);
  expect(injectorMock.getInjectable().dispatch.mock.calls[0]).toEqual([actionTypes.REQUEST_USER, undefined]);
  expect(injectorMock.getInjectable().dispatch.mock.calls[1]).toEqual([actionTypes.REQUEST_USER_FAILED, undefined]);
});
