const _storage = {}

const localStorage = {
  getItem: (key) => _storage[key] || null,
  setItem: (key, value) => _storage[key] = value,
  removeItem: (key) => delete _storage[key],
}

export default localStorage
