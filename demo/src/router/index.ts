import Vue from 'vue';
import Router from 'vue-router';
// eslint-disable-next-line import/no-unresolved
import { Route, VueRouter, NavigationGuard } from 'vue-router/types/router';
import LoginPage from '@/pages/Login/Index';
import AuthPage from '@/pages/Authenticate/Index';
import IndexPage from '@/pages/Index/Index';
import DetailsPage from '@/pages/Detail/Index';
import * as authConnector from '@/connectors/auth';

Vue.use(Router);

const logoutAction: NavigationGuard = (to: Route, from: Route, next: Function): void => {
  authConnector.removeTokens();
  next('/login');
};

const isAuthenticated: NavigationGuard = (to: Route, from: Route, next: Function): void => {
  if (authConnector.hasAuth()) {
    next();
  } else {
    next('/login');
  }
};

const isNotAuthenticated: NavigationGuard = (to: Route, from: Route, next: Function): void => {
  if (!authConnector.hasAuth()) {
    next();
  } else {
    next('/users');
  }
};

const router: VueRouter = new Router({
  mode: 'history',
  routes: [
    {
      path: '/logout',
      name: 'logout',
      // remove sensitive information and route to /login
      beforeEnter: logoutAction,
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage,
      // if authenticated redirect to /users
      beforeEnter: isNotAuthenticated,
    },
    {
      path: '/callback',
      name: 'logincallback',
      component: AuthPage,
      // if authenticated redirect to /users
      beforeEnter: isNotAuthenticated,
    },
    {
      path: '/users',
      name: 'users',
      component: IndexPage,
      // if not authenticated redirect to /login
      beforeEnter: isAuthenticated,
    },
    {
      path: '/users/:id',
      name: 'details',
      component: DetailsPage,
      // if not authenticated redirect to /login
      beforeEnter: isAuthenticated,
    },
    // redirect everything unknown to /users
    // /users will take care of authentication middleware
    {
      path: '*',
      redirect: { name: 'users' },
    },
  ],
});

export default router;
