enum StateStatus {
  IDLE = 'IDLE',
  PENDING = 'PENDING',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

enum Injectables {
  STORE = 'STORE',
}

export {
  StateStatus,
  Injectables
}