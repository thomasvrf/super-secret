import * as ENUMS from './enums';

interface StringMap {
  [key: string]: string
}

interface StringNumberMap {
  [key: string]: number
}

interface UsersStore {
  status: ENUMS.StateStatus,
  pagesLoaded: number,
  totalRows: number,
  limit: number,
  users: Array<User>,
  query: string,
}

interface UserStore {
  status: ENUMS.StateStatus,
  user: {} | UserDetail,
}

interface CommuteStore {
  status: ENUMS.StateStatus,
  commutes: Array<Commute>,
}

interface User {
  id: number,
  name: string,
  email: string,
  role_name: string,
}

interface UsersData {
  total_record_count: number,
  data: Array<User>
}

interface UserDetailAddress {
  city: string,
  country_code: string,
  line_1: string,
  line_2: string,
  state: string,
  zipcode: string,
}

interface UserDetail {
  id: number,
  name: string,
  email: string,
  address: UserDetailAddress,
}

interface DailyTimeEngagement {
  day: string,
  dayDuration: number,
}

interface ConnectionDetail {
  station: string
}

interface Connection {
  id: string,
  duration: number,
  arrival: ConnectionDetail,
  departure: ConnectionDetail
}

interface Commute {
  day: string,
  date: Date,
  address: string,
  hours: number,
  arrival: null | Connection,
  departure: null | Connection,
}

export {
  StringMap,
  StringNumberMap,
  UsersStore,
  User,
  UsersData,
  UserStore,
  UserDetailAddress,
  UserDetail,
  DailyTimeEngagement,
  Connection,
  Commute,
  CommuteStore,
};
