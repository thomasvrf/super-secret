import 'whatwg-fetch';
import Vue from 'vue';
import { Injectables } from '@/types/enums';
import App from './App.vue';
import router from './router';
import store from './store';
import { setInjectable } from './utils/injector';

setInjectable(Injectables.STORE, store);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
});
