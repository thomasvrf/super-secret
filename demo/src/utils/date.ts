const getDaysInWeek = (from: Date): Array<Date> => {
  const day = from.getDay();

  // if 0 (sunday): substract a full week from date
  // if not: substract current day (+1 to adjust for sunday)
  const offset = (from.getDate() - day) + (day === 0 ? -6 : 1);

  // Array.from Array(7): create iterable array to map for every day
  const days = Array.from(Array(7))
    // new Date automatically shifts months
    .map((_, i) => new Date(from.getFullYear(), (from.getMonth()), (offset + i)));

  return days;
};

const getEndOfDay = (start: number, time: number, br: number = 0): string => {
  // calc with minutes
  const result = start + time + br;

  // (get it to mintues, move and lose decimals)
  const eod = Math.round(((result / 60) * 100));

  return `${eod}`;
};

const parseDateToIRailsFormat = (date: Date): string => {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = `${date.getFullYear()}`.slice(-2);

  const normalizedDay = day < 10 ? `0${day}` : `${day}`;
  const normalizedMonth = month < 10 ? `0${month}` : `${month}`;

  return `${normalizedDay}${normalizedMonth}${year}`;
};

const secondsToHumanReadable = (seconds) => {
  const hours = Math.floor(seconds / 3600);
  const minuts = Math.floor((seconds - (hours * 3600)) / 60);

  const normalizedHours = hours < 10 ? `0${hours}h` : `${hours}h`;
  const normalizedMinutes = minuts < 10 ? `0${minuts}m` : `${minuts}m`;

  const HHMM = `${normalizedHours}:${normalizedMinutes}`;

  return HHMM;
};

export {
  getDaysInWeek,
  getEndOfDay,
  parseDateToIRailsFormat,
  secondsToHumanReadable,
};
