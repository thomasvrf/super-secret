const injectables = {};
const setInjectable = (name: string, instance: any): void => injectables[name] = instance;
const getInjectable = (name: string): any => injectables[name];

export {
  setInjectable,
  getInjectable,
};
