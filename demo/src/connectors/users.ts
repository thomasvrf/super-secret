import { UsersStore, UsersData, User } from '@/types/index';
import { StateStatus, Injectables } from '@/types/enums';
import actionsTypes from '@/store/actions';
import { getInjectable } from '@/utils/injector';
import * as userService from '@/services/api/officient/users';

const dispatch = (action: string, payload?: any): void => getInjectable(Injectables.STORE).dispatch(action, payload);
const getStateModule = (): UsersStore => getInjectable(Injectables.STORE).state.users;

/*
 * Getters
 */

const getPages = (): number => getStateModule().pagesLoaded;

const getStatus = (): StateStatus => getStateModule().status;

const canLoadMore = (): boolean => {
  const userState: UsersStore = getStateModule();
  return (userState.totalRows > (userState.pagesLoaded * userState.limit));
};

const getUsers = (): Array<User> => {
  const users: UsersStore = getStateModule();

  return users.users.filter((user: User): boolean =>
    // make compare case-insensitive
    user.name.toLowerCase().includes(users.query));
};

const twoWayQuery = () => ({
  get: (): string => getStateModule().query,
  set: (query: string): void => dispatch(actionsTypes.USERS_SEARCH_QUERY, { query }),
});

/*
 * Actions
 */

const fetchPage = async (page: number, onSuccess: Function): Promise<void> => {
  dispatch(actionsTypes.REQUEST_USERS);

  try {
    const users: UsersData = await userService.getUsers(page);
    onSuccess(users);
  } catch (e) {
    dispatch(actionsTypes.REQUEST_USERS_FAILED);
  }
};


const fetchInitialUsers = async (): Promise<void> => {
  await fetchPage(0, (users: UsersData) => {
    dispatch(actionsTypes.REQUEST_USERS_SUCCESS, {
      users: users.data,
      record_count: users.total_record_count,
    });
  });
};

const fetchMoreUsers = async (page: number): Promise<void> => {
  await fetchPage(page, (users: UsersData) => {
    dispatch(actionsTypes.REQUEST_MORE_USERS_SUCCESS, {
      users: users.data,
      record_count: users.total_record_count,
    });
  });
};

export {
  fetchInitialUsers,
  fetchMoreUsers,
  getUsers,
  getPages,
  getStatus,
  canLoadMore,
  twoWayQuery,
};
