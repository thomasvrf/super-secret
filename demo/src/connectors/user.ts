import { UserStore, UserDetail } from '@/types/index';
import { StateStatus, Injectables } from '@/types/enums';
import actionTypes from '@/store/actions';
import { getInjectable } from '@/utils/injector';
import * as userService from '@/services/api/officient/users';
import * as commuteConnector from './commute';

const dispatch = (action: string, payload?: any): void => getInjectable(Injectables.STORE).dispatch(action, payload);
const getStateModule = (): UserStore => getInjectable(Injectables.STORE).state.user;

/*
 * Getters
 */

const getUser = (): {} | UserDetail => getStateModule().user;
const getUserInfoStatus = (): StateStatus => getStateModule().status;

/*
 * Actions
 */

const fetchUser = async (id: number): Promise<void> => {
  // fetching a user will reset commute
  commuteConnector.reset();
  dispatch(actionTypes.REQUEST_USER);

  try {
    const user: UserDetail = await userService.getUser(id);

    dispatch(actionTypes.REQUEST_USER_SUCCESS, { user });
    commuteConnector.fetchConnections(id, user.address.city);
  } catch (e) {
    dispatch(actionTypes.REQUEST_USER_FAILED);
  }
};

export {
  fetchUser,
  getUser,
  getUserInfoStatus,
};
