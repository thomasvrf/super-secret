import { DailyTimeEngagement, Commute, CommuteStore, Connection } from '@/types/index';
import { StateStatus, Injectables } from '@/types/enums';
import actionsTypes from '@/store/actions';
import { getInjectable } from '@/utils/injector';
import { getDaysInWeek, getEndOfDay, parseDateToIRailsFormat } from '@/utils/date';
import * as wageService from '@/services/api/officient/wages';
import * as connectionService from '@/services/api/irail/connections';

const OFFICE_LOCATION: string = 'Gent-Sint-Pieters';
const OFFICE_START_TIME: number = 9;
const OFFICE_START_TIME_IRAILS: string = '0900';

const dispatch = (action: string, payload?: any): void => getInjectable(Injectables.STORE).dispatch(action, payload);
const getStateModule = (): CommuteStore => getInjectable(Injectables.STORE).state.commute;

/*
 * Getters
 */

const userCommuteStatus = (): StateStatus => getStateModule().status;
const getCommutes = (): Array<Commute> => getStateModule().commutes;

/*
 * Actions
 */

const reset = (): void => dispatch(actionsTypes.RESET_USER_COMMUTE);

const getCommuteRequest = async (timeEngagement: DailyTimeEngagement, date: Date, address: string): Promise<Commute> => {
  const commuteObject: Commute = {
    date,
    address,
    day: timeEngagement.day,
    hours: timeEngagement.dayDuration,
    arrival: null,
    departure: null,
  };

  const IRailsDate: string = parseDateToIRailsFormat(date);
  // convert hour to minutes, add engagement, and add a lunchbreak;
  const IRailsHour: string = getEndOfDay((OFFICE_START_TIME * 60), timeEngagement.dayDuration, 60);

  // let arrivals & departures fail individually
  try {
    const arrivals: Array<Connection> = await connectionService.getConnections(address, OFFICE_LOCATION, IRailsDate, OFFICE_START_TIME_IRAILS, 'arrival');
    commuteObject.arrival = arrivals[0] || null;
  } catch (e) { /* swallow err, use default object */ }

  try {
    const departures: Array<Connection> = await connectionService.getConnections(OFFICE_LOCATION, address, IRailsDate, IRailsHour, 'departure');
    commuteObject.departure = departures[0] || null;
  } catch (e) { /* swallow err, use default object */ }

  return commuteObject;
};

const fetchConnections = async (id: number, address: string): Promise<void> => {
  dispatch(actionsTypes.REQUEST_USER_COMMUTE);

  try {
    const timeEngagement: Array<DailyTimeEngagement> = await wageService.getUserWage(id);

    const days: Array<Date> = getDaysInWeek(new Date());

    const commutes: Array<Commute> = [];
    // fire all async, await all: not working, irails not happy with requests
    // await Promise.all(timeEngagement.map((day, i) => getCommuteRequest(...))
    for (let i = 0; i < 7; i++) {
      // eslint-disable-next-line no-await-in-loop
      commutes[i] = await getCommuteRequest(timeEngagement[i], days[i], address);
    }

    dispatch(actionsTypes.REQUEST_USER_COMMUTE_SUCCESS, { commutes });
  } catch (e) {
    dispatch(actionsTypes.REQUEST_USER_COMMUTE_FAILED);
  }
};

export {
  fetchConnections,
  getCommutes,
  userCommuteStatus,
  reset,
};
