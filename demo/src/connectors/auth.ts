import * as authService from '@/services/api/officient/auth';
import * as tokenService from '@/services/local-storage/tokens';

const hasAuth = (): boolean => (tokenService.hasAccessToken() && tokenService.hasRefreshToken());

const removeTokens = (): void => tokenService.removeTokens();

const auth = async (code: string): Promise<void> => {
  const authCodes: any = await authService.auth(code);

  // officient api /token always returns a 200
  // (not caught in http default service, handle here)
  if (authCodes.error) {
    throw new Error(authCodes.error_description);
  } else {
    const { access_token, refresh_token } = authCodes;
    tokenService.setTokens(access_token, refresh_token);
  }
};

export {
  hasAuth,
  auth,
  removeTokens,
};
