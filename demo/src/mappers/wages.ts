import { DailyTimeEngagement } from '@/types/index';

const mapTimeEngagementDay = (day: string, engagement: number): DailyTimeEngagement => ({
  day,
  dayDuration: (engagement || 0),
});

// transform to (ordered) iterable Array
const mapWageTimeEngagement = (wage: any): Array<DailyTimeEngagement> => ([
  mapTimeEngagementDay('monday', wage.weekly_time_engagement_minutes.monday),
  mapTimeEngagementDay('tuesday', wage.weekly_time_engagement_minutes.tuesday),
  mapTimeEngagementDay('wednesday', wage.weekly_time_engagement_minutes.wednesday),
  mapTimeEngagementDay('thursday', wage.weekly_time_engagement_minutes.thursday),
  mapTimeEngagementDay('friday', wage.weekly_time_engagement_minutes.friday),
  mapTimeEngagementDay('saturday', wage.weekly_time_engagement_minutes.saturday),
  mapTimeEngagementDay('sunday', wage.weekly_time_engagement_minutes.sunday),
]);

export {
  mapWageTimeEngagement,
};
