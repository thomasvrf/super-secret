import { Connection } from '@/types/index';

const mapConnection = (connection): Connection => ({
  id: connection.id,
  duration: connection.duration,
  arrival: {
    station: connection.arrival.station,
  },
  departure: {
    station: connection.departure.station,
  },
});

const mapConnections = (rawData): Array<Connection> => rawData.map(mapConnection);

export {
  mapConnections,
};
