import { UsersData, User, UserDetail } from '@/types/index';

const mapToUser = (user: any): User => ({
  id: user.id,
  name: user.name,
  email: (user.email || ''),
  role_name: (user.role_name || ''),
});

const mapToUserDetail = (rawData: any): UserDetail => ({
  id: rawData.id,
  name: rawData.name,
  email: (rawData.email || ''),
  address: {
    city: ((rawData.address && rawData.address.city) || ''),
    country_code: ((rawData.address && rawData.address.country_code) || ''),
    line_1: ((rawData.address && rawData.address.line_1) || ''),
    line_2: ((rawData.address && rawData.address.line_2) || ''),
    state: ((rawData.address && rawData.address.state) || ''),
    zipcode: ((rawData.address && rawData.address.zipcode) || ''),
  },
});

const mapToUsersData = (rawData: any): UsersData => ({
  total_record_count: rawData.total_record_count,
  data: rawData.data.map(mapToUser),
});

export {
  mapToUsersData,
  mapToUserDetail,
};
