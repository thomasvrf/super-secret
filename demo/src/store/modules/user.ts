/* eslint no-param-reassign: 0 */
// eslint-disable-next-line import/no-unresolved
import { ActionTree, MutationTree } from 'vuex/types/index';
import { UserStore } from '@/types/index';
import { StateStatus } from '@/types/enums';
import actionsTypes from '@/store/actions';

const defaultState: UserStore = {
  status: StateStatus.IDLE,
  user: {},
};

// clone the defaultState to a mutable (initial) vuex state
const state: UserStore = { ...defaultState };

// actions
const actions: ActionTree<UserStore, any> = {
  [actionsTypes.REQUEST_USER]({ commit }): void {
    commit(actionsTypes.REQUEST_USER);
  },

  [actionsTypes.REQUEST_USER_FAILED]({ commit }): void {
    commit(actionsTypes.REQUEST_USER_FAILED);
  },

  [actionsTypes.REQUEST_USER_SUCCESS]({ commit }, payload: any): void {
    commit(actionsTypes.REQUEST_USER_SUCCESS, payload);
  },
};

const mutations: MutationTree<UserStore> = {
  [actionsTypes.REQUEST_USER](currentState: UserStore): void {
    currentState.status = StateStatus.PENDING;
  },

  [actionsTypes.REQUEST_USER_FAILED](currentState: UserStore): void {
    currentState.status = StateStatus.FAILED;
  },

  [actionsTypes.REQUEST_USER_SUCCESS](currentState: UserStore, payload: any): void {
    currentState.status = StateStatus.SUCCESS;
    currentState.user = payload.user;
  },
};

export default {
  state,
  actions,
  mutations,
};
