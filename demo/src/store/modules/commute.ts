/* eslint no-param-reassign: 0 */
// eslint-disable-next-line import/no-unresolved
import { ActionTree, MutationTree } from 'vuex/types/index';
import { CommuteStore } from '@/types/index';
import { StateStatus } from '@/types/enums';
import actionsTypes from '@/store/actions';

const defaultState: CommuteStore = {
  status: StateStatus.IDLE,
  commutes: [],
};

// clone the defaultState to initial (mutable) vuex state
const state: CommuteStore = { ...defaultState };

// actions
const actions: ActionTree<CommuteStore, string> = {
  [actionsTypes.RESET_USER_COMMUTE]({ commit }): void {
    commit(actionsTypes.RESET_USER_COMMUTE);
  },

  [actionsTypes.REQUEST_USER_COMMUTE]({ commit }): void {
    commit(actionsTypes.REQUEST_USER_COMMUTE);
  },

  [actionsTypes.REQUEST_USER_COMMUTE_SUCCESS]({ commit }, payload): void {
    commit(actionsTypes.REQUEST_USER_COMMUTE_SUCCESS, payload);
  },

  [actionsTypes.REQUEST_USER_COMMUTE_FAILED]({ commit }, payload): void {
    commit(actionsTypes.REQUEST_USER_COMMUTE_FAILED, payload);
  },
};

const mutations: MutationTree<CommuteStore> = {
  [actionsTypes.RESET_USER_COMMUTE](currentState: CommuteStore): void {
    currentState.status = StateStatus.IDLE;
    currentState.commutes = [];
  },

  [actionsTypes.REQUEST_USER_COMMUTE](currentState: CommuteStore): void {
    currentState.status = StateStatus.PENDING;
    currentState.commutes = [];
  },

  [actionsTypes.REQUEST_USER_COMMUTE_FAILED](currentState: CommuteStore): void {
    currentState.status = StateStatus.FAILED;
  },

  [actionsTypes.REQUEST_USER_COMMUTE_SUCCESS](currentState: CommuteStore, payload: any): void {
    currentState.status = StateStatus.SUCCESS;
    currentState.commutes = payload.commutes;
  },
};

export default {
  state,
  actions,
  mutations,
};
