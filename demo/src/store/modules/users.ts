/* eslint no-param-reassign: 0 */
// eslint-disable-next-line import/no-unresolved
import { ActionTree, MutationTree } from 'vuex/types/index';
import { UsersStore } from '@/types/index';
import { StateStatus } from '@/types/enums';
import actionsTypes from '@/store/actions';

const defaultState: UsersStore = {
  status: StateStatus.IDLE,
  pagesLoaded: 0,
  limit: 30,
  totalRows: 0,
  users: [],
  query: '',
};

// clone the defaultState to a mutable (initial) vuex state
const state: UsersStore = { ...defaultState };

// actions
const actions: ActionTree<UsersStore, any> = {
  [actionsTypes.REQUEST_USERS]: ({ commit }): void => {
    commit(actionsTypes.REQUEST_USERS);
  },

  [actionsTypes.REQUEST_USERS_SUCCESS]({ commit }, payload: any): void {
    commit(actionsTypes.REQUEST_USERS_SUCCESS, payload);
  },

  [actionsTypes.REQUEST_USERS_FAILED]({ commit }, payload: any): void {
    commit(actionsTypes.REQUEST_USERS_FAILED, payload);
  },

  [actionsTypes.USERS_SEARCH_QUERY]({ commit }, payload: any): void {
    commit(actionsTypes.USERS_SEARCH_QUERY, payload);
  },

  [actionsTypes.REQUEST_MORE_USERS_SUCCESS]({ commit }, payload: any): void {
    commit(actionsTypes.REQUEST_MORE_USERS_SUCCESS, payload);
  },
};

// mutations
const mutations: MutationTree<UsersStore> = {
  [actionsTypes.REQUEST_USERS](currentState: UsersStore): void {
    currentState.status = StateStatus.PENDING;
  },

  [actionsTypes.REQUEST_USERS_SUCCESS](currentState: UsersStore, payload: any): void {
    currentState.status = StateStatus.SUCCESS;
    currentState.pagesLoaded += 1;
    currentState.users = payload.users;
    currentState.totalRows = payload.record_count;
  },

  [actionsTypes.REQUEST_MORE_USERS_SUCCESS](currentState: UsersStore, payload: any): void {
    currentState.status = StateStatus.SUCCESS;
    currentState.pagesLoaded += 1;
    currentState.users = currentState.users.concat(payload.users);
    currentState.totalRows = payload.record_count;
  },

  [actionsTypes.USERS_SEARCH_QUERY](currentState: UsersStore, payload: any): void {
    currentState.query = payload.query;
  },

  [actionsTypes.REQUEST_USERS_FAILED](currentState: UsersStore): void {
    currentState.status = StateStatus.FAILED;
  },
};

export default {
  state,
  actions,
  mutations,
};
