const set = (key: string, value: any): void => localStorage.setItem(key, value);
const get = (key: string): any => localStorage.getItem(key);
const remove = (key: string): void => localStorage.removeItem(key);

export {
  get,
  set,
  remove,
};
