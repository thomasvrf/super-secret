import * as lsService from './index';

const ACCESS_TOKEN: string = 'off:access_token';
const REFRESH_TOKEN: string = 'off:refresh_token';

const getAccessToken = (): string => (lsService.get(ACCESS_TOKEN) || '');
const getRefreshToken = (): string => (lsService.get(REFRESH_TOKEN) || '');

const hasAccessToken = (): boolean => !!lsService.get(ACCESS_TOKEN);
const hasRefreshToken = (): boolean => !!lsService.get(REFRESH_TOKEN);

const setTokens = (accessToken: string, refreshToken: string): void => {
  lsService.set(ACCESS_TOKEN, accessToken);
  lsService.set(REFRESH_TOKEN, refreshToken);
};

const setAccessToken = (accessToken: string): void => {
  lsService.set(ACCESS_TOKEN, accessToken);
};

const removeTokens = (): void => {
  lsService.remove(ACCESS_TOKEN);
  lsService.remove(REFRESH_TOKEN);
};

export {
  setTokens,
  setAccessToken,
  removeTokens,
  getAccessToken,
  getRefreshToken,
  hasAccessToken,
  hasRefreshToken,
};
