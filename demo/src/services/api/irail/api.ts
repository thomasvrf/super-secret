import { fetchRefreshRetry } from '@/services/api/http';

const basePath: string = 'https://api.irail.be';

const apiRequest = (path: string, reqObject: any): any => {
  const fullPath: string = `${basePath}${path}`;
  return fetchRefreshRetry(fullPath, reqObject);
};

export {
  apiRequest,
};
