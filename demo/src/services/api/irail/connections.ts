import { mapConnections } from '@/mappers/connections';
import { Connection } from '@/types/index';
import { apiRequest } from './api';

const getConnections = async (from: string, to: string, date: string, time: string, timesel: string): Promise<Array<Connection>> => {
  const rawData = await apiRequest(`/connections/?from=${from}&to=${to}&date=${date}&time=${time}&timesel=${timesel}&format=json&lang=en&results=1`, {
    method: 'GET',
  });

  return mapConnections(rawData.connection);
};

export {
  getConnections,
};
