import { mapWageTimeEngagement } from '@/mappers/wages';
import { DailyTimeEngagement } from '@/types/index';
import { apiRequest } from './api';

const getUserWage = async (id: number): Promise<Array<DailyTimeEngagement>> => {
  const rawData = await apiRequest(`/wages/${id}/current`, {
    method: 'GET',
  });

  return mapWageTimeEngagement(rawData.data);
};

export {
  getUserWage,
};
