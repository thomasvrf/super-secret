import { UsersData, UserDetail } from '@/types/index';
import { mapToUsersData, mapToUserDetail } from '@/mappers/users';
import { apiRequest } from './api';

const getUsers = async (page: number): Promise<UsersData> => {
  const rawData: any = await apiRequest(`/people/list?page=${page}`, {
    method: 'GET',
  });

  return mapToUsersData(rawData);
};

const getUser = async (id: number): Promise<UserDetail> => {
  const rawData: any = await apiRequest(`/people/${id}/detail`, {
    method: 'GET',
  });

  return mapToUserDetail(rawData.data);
};

export {
  getUsers,
  getUser,
};
