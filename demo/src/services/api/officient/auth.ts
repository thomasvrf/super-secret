import { setAccessToken, getRefreshToken } from '@/services/local-storage/tokens';
import { apiRequest } from './api';

const auth = (code): any => apiRequest('/token', {
  method: 'POST',
  body: JSON.stringify({
    code,
    grant_type: 'authorization_code',
    refresh_token: '',
  }),
});

const refresh = async (): Promise<string> => {
  const refreshToken = getRefreshToken();

  const token = await apiRequest('/token', {
    method: 'POST',
    body: JSON.stringify({
      code: '',
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
    }),
  });

  const accessToken = (token.access_token || '');
  setAccessToken(accessToken);
  return accessToken;
};

export {
  auth,
  refresh,
};
