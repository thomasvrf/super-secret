import { fetchRefreshRetry } from '@/services/api/http';
import { getAccessToken } from '@/services/local-storage/tokens';
import { refresh } from './auth';

const basePath: string = 'http://localhost:3001/api/officient';

const apiRequest = (path: string, reqObject: any): any => {
  const fullPath: string = `${basePath}${path}`;

  const reqObjectExtended: any = { ...reqObject,
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
      'Content-Type': 'application/json',
    },
  };

  return fetchRefreshRetry(fullPath, reqObjectExtended, refresh, true)
    // catch early for 401s
    .catch((err) => {
      if (err.code === 401) {
        // if we tried to refresh the token, and we still get 401: byebye
        window.location.assign('/logout');
      } else {
        // else, keep the error going, should not be handled here
        throw err;
      }
    });
};

export {
  apiRequest,
};
