function hasError(response: any): any {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  // eslint-disable-next-line
  error['code'] = response.status;
  throw error;
}

const fetchRefreshRetry = (path: string, reqObject: any, refreshFn?: Function, refresh: boolean = false): Promise<any> => fetch(path, reqObject)
  // throw error when statusCode indicates eeor
  .then(hasError)
  // parse response
  .then((response): any => response.json())
  // refresh once if specified
  .catch((err) => {
    if (err.code === 401 && refreshFn && refresh) {
      return refreshFn()
          .then((newToken) => {
            // recursive
            // eslint-disable-next-line no-param-reassign
            reqObject.headers.Authorization = `Bearer ${newToken}`;
            return fetchRefreshRetry(path, reqObject);
          })
          // if even the refresh fails, throw 401 immediately
          .catch((refreshErr) => {
            const error = new Error(refreshErr);
            // eslint-disable-next-line
            error['code'] = 401;
            throw error;
          });
    }
    throw err;
  });

export {
  fetchRefreshRetry,
};
