# Demo client

> Vue + Vuex + TypeScript + Jest + ESLint

TypeScript is implemented in a very non-strict way. Basically to have better types and function headers.  Vuex implemented with an alternative pattern to connect views to services to state (see more later).

## Build API

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# run linter
npm run lint

# run unit tests
npm run unit

# run all tests
npm test (npm run lint && npm run unit)

# build for production with minification
npm run build
```

## Alternative Vuex

In a default implemented Vuex way we would use `$store.state.prop` in the view layer, for *computed* properties. And we would call services in the *actions* (store).

Trying out: *connectors*:

A *connector* is thin-ish layer between the view and the services (localstorage, vuex, apis, ...), the connector delegates which services should be called; for example:

``` javascript
apiFetchUser()
    .then(setVuexStateUser)
    .then(localStorageSetUser)
```

This means every service is completed decoupled and has a single responsibility, swapping out technologies and frameworks can be done without changing anything else.
In an ideal world this would mean switching from Vue to React should only impact */views*, switching from Vuex to Redux should only impact */store*.
