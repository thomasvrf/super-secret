# Demo: Commute Sheet

> Demo: Commute Sheet: client + (simple) server

## Run (after installing deps)

**Note**: More information to be found in separate projects

``` bash
$ node server/index.js &

$ cd demo/

$ npm run dev
```

**Note**: For the server you will need a *.env* with your application (API) secrets: 

* CLIENT_ID=XYZ
* CLIENT_SECRET=XYZ