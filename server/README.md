# Demo server

> Express

To keep application secrets safe from the front-end.

``` bash
$ npm install

$ node index.js
```

Has an *.env* in format:

```
CLIENT_ID=XYZ
CLIENT_SECRET=XYZ
```