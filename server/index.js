// very simple and basic proxy server for the front-end
// keep the app secrets safe from the client-side

const request = require("request");
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config()

const PORT = 3001;
const clientID = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

const app = express();
app.use(bodyParser.json());
app.disable('x-powered-by');
// ((development mode, no CORS))
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

// https://api.officient.io/1.0/token
app.post('/api/officient/token', (req, res) => {
  const { code, refresh_token, grant_type } = req.body;

  const reqOptions = {
    method: 'POST',
    url: 'https://api.officient.io/1.0/token',
    formData: {
      code,
      refresh_token,
      grant_type,
      client_id: clientID,
      client_secret: clientSecret
    }
  }

  request(reqOptions, (err, response, body) => {
    if (err) return res.status(500).send(err);
    res.status(response.statusCode).send(body);
  });
});


// https://api.officient.io/1.0/people/list?page={n}
app.get('/api/officient/people/list', (req,res) => {
  const page = req.query.page;

  let token = '';
  if (req.headers && req.headers.authorization) {
    token = req.headers.authorization.split(' ')[1]
  }

  const reqOptions = {
    method: 'GET',
    url: `https://api.officient.io/1.0/people/list?page=${page}`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  request(reqOptions, (err, response, body) => {
    if (err) return res.status(500).send(err);
    res.status(response.statusCode).send(body);
  });
})

// https://api.officient.io/1.0/people/:id/detail
app.get('/api/officient/people/:id/detail', (req,res) => {
  const id = req.params.id;

  let token = '';
  if (req.headers && req.headers.authorization) {
    token = req.headers.authorization.split(' ')[1]
  }

  const reqOptions = {
    method: 'GET',
    url: `https://api.officient.io/1.0/people/${id}/detail`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  request(reqOptions, (err, response, body) => {
    if (err) return res.status(500).send(err);
    res.status(response.statusCode).send(body);
  });
})

// https://api.officient.io/1.0/wages/:id/current
app.get('/api/officient/wages/:id/current', (req,res) => {
  const id = req.params.id;

  let token = '';
  if (req.headers && req.headers.authorization) {
    token = req.headers.authorization.split(' ')[1]
  }

  const reqOptions = {
    method: 'GET',
    url: `https://api.officient.io/1.0/wages/${id}/current`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }

  request(reqOptions, (err, response, body) => {
    if (err) return res.status(500).send(err);
    res.status(response.statusCode).send(body);
  });
});


app.listen(PORT, (err) => {
  if (err) console.log('could not boot server', err);
});
